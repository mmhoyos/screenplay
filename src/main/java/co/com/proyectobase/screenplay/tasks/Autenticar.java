package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.userinterface.MaxTimeLoginPage;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Autenticar implements Task {
	
	
	
	private MaxTimeLoginPage maxtimeLoginPage;
	private DataTable dtDatos;

	public Autenticar(DataTable dtDatos) {
		this.dtDatos = dtDatos;
	}
	
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(maxtimeLoginPage));
		List<List<String>> data = dtDatos.raw();
		actor.attemptsTo(Enter.theValue(data.get(0).get(0).trim()).into(MaxTimeLoginPage.USUARIO));
		actor.attemptsTo(Enter.theValue(data.get(0).get(1).trim()).into(MaxTimeLoginPage.CONTRASENA));
		actor.attemptsTo(Click.on(MaxTimeLoginPage.BTNCONECTARSE));
		
	}

	public static Performable EnLaAplicacionMaxtime(DataTable dtDatos) {
		return Tasks.instrumented(Autenticar.class, dtDatos);
	}
	

	
	

}
