package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.GoogleHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Traducir implements Task{
	
	private String palabra;
	
	public Traducir(String palabra) {
		super();
		this.palabra = palabra;
	}


	@Override
	public <T extends Actor> void performAs(T actor) {
				
		actor.attemptsTo(Click.on(GoogleHomePage.BOTON_APLICACIONES));	
	  	actor.attemptsTo(Click.on(GoogleHomePage.BOTON_GOOGLE_TRASLATE));
		actor.attemptsTo(Click.on(GoogleHomePage.BOTON_LENGUAJE_ORIGEN));
		actor.attemptsTo(Click.on(GoogleHomePage.OPCION_INGLES));
		actor.attemptsTo(Click.on(GoogleHomePage.BOTON_LENGUAJE_DESTINO));
		actor.attemptsTo(Click.on(GoogleHomePage.OPCION_ESPAÑOL));
		actor.attemptsTo(Enter.theValue(palabra).into(GoogleHomePage.AREA_DE_TRADUCCION));
		actor.attemptsTo(Click.on(GoogleHomePage.BOTON_TRADUCIR));
		
	}
	
	
	public static Traducir DeInglesAEspanolLa(String palabra) {
		return Tasks.instrumented(Traducir.class, palabra);
	}


}
