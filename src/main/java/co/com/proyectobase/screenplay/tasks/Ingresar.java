package co.com.proyectobase.screenplay.tasks;

import net.serenitybdd.screenplay.waits.WaitUntil; 
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible; 
import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.userinterface.MaxTimeLoginPage;
import cucumber.api.DataTable;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;


public class Ingresar implements Task {

	// Linea para el manejo de Iframe
	private static WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver(); 

	private MaxTimeLoginPage maxtimeLoginPage;
	private List<String> dtDatos;

	public Ingresar(List<String> dtDatos) {
		this.dtDatos = dtDatos;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
			
//		actor.attemptsTo(Esperar.aMoment());
//		actor.attemptsTo(Esperar.aMoment());
		
		actor.attemptsTo(WaitUntil.the(MaxTimeLoginPage.FECHA, isVisible()).
				forNoMoreThan(10).seconds());
//	    WaitUntil.the(MaxTimeLoginPage.FECHA,isVisible());	    
		actor.attemptsTo(Click.on(MaxTimeLoginPage.FECHA));		
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());		
		WaitUntil.the(MaxTimeLoginPage.DIA_LABORAL,isVisible());
				
		if (MaxTimeLoginPage.DIA_LABORAL.resolveFor(actor).getAttribute("value").equals("C")) {
			actor.attemptsTo(Esperar.aMoment());
			
			actor.attemptsTo(Click.on(MaxTimeLoginPage.BTNNUEVO));
			actor.attemptsTo(Click.on(MaxTimeLoginPage.PROYECTO.resolveAllFor(actor).get(0)));
			
			driver.switchTo().frame(MaxTimeLoginPage.FRAME.resolveAllFor(actor).get(0).getAttribute("id")); // Linea que va antes de que se cargue el iframe 
			actor.attemptsTo(Enter.theValue(dtDatos.get(0).trim()).into(MaxTimeLoginPage.IPROYECTO));
			actor.attemptsTo(Click.on(MaxTimeLoginPage.BTPROYECTO));
			actor.attemptsTo(Click.on(MaxTimeLoginPage.SPROYECTO));
			actor.attemptsTo(Click.on(MaxTimeLoginPage.BTNACEPTAR));
			driver.switchTo().defaultContent(); // Linea que va despues de trabajar con el iframe 
			
		}
	
		actor.attemptsTo(Click.on(MaxTimeLoginPage.BTNCERRARDIA));
		driver.switchTo().frame(0); // Linea que va antes de que se cargue el iframe 
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(MaxTimeLoginPage.BTN_CONF_CIERRE));		
		driver.switchTo().defaultContent(); // Linea que va despues de trabajar con el iframe 
		
	
	}

	public static Ingresar LasActividadesDelDia(List<String> dtDatos) {
			return Tasks.instrumented(Ingresar.class, dtDatos);
	}

}
