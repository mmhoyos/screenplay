package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.choucairtesting.com:18000/MaxtimeCHC/Login.aspx")
public class MaxTimeLoginPage extends PageObject {

	public static final Target USUARIO = Target.the("Usuario")
			.located(By.id("Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I"));
	public static final Target CONTRASENA = Target.the("Contraseña")
			.located(By.id("Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I"));
	public static final Target BTNCONECTARSE = Target.the("bton conectarse")
			.located(By.id("Logon_PopupActions_Menu_DXI0_T"));

	public static final Target ANALISTA = Target.the("Analista").located(By.id("Vertical_v1_LE_v2_HFListBox_LBI3T0"));
	public static final Target FECHA = Target.the("Fecha").located(By.id("Vertical_v1_LE_v2_tccell0_0"));
	public static final Target DIA_LABORAL = Target.the("bton dia laboral")
			.located(By.id("Vertical_v3_MainLayoutView_xaf_l59_xaf_dviLaboral_View_S"));
	public static final Target BTNNUEVO = Target.the("bton nuevo").located(By.xpath(
			"//*[@id='Vertical_v3_MainLayoutView_xaf_l103_xaf_dviReporteDetallado_UPToolBar']//li[contains(@id,'DXI0')]"));
	public static final Target PROYECTO = Target.the("bton proyecto")
			.located(By.className("dxbButton_Office2010Blue"));
	
	public static final Target IPROYECTO = Target.the("ingresar pmo del proyecto")
			.located(By.id("Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_Ed_I"));
							
	public static final Target BTPROYECTO = Target.the("Buscar proyecto")
			.located(By.id("Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_CD"));
		public static final Target SPROYECTO = Target.the("Seleccionar proyecto")
			.located(By.id("Dialog_v7_LE_v8_DXSelBtn0_D"));
	public static final Target BTNACEPTAR = Target.the("boton aceptar")
			.located(By.id("Dialog_actionContainerHolder_Menu_DXI0_T"));
	
	public static final Target BTNCERRARDIA = Target.the("boton cerrar dia")
			.located(By.id("Vertical_TB_Menu_DXI1_"));
	
	public static final Target BTN_CONF_CIERRE = Target.the("boton cerrar dia")
			.located(By.id("Dialog_actionContainerHolder_Menu_DXI0_T_"));
	
	public static final Target FRAME = Target.the("El frame de las opciones.").
			located(By.className("dxpc-iFrame"));
	

}
