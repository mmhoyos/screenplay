package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.google.com")
public class GoogleHomePage extends PageObject {
	
	public static final Target BOTON_APLICACIONES=Target.the("El botón que muestra las aplicaciones")
			.located(By.id("gb51"));
	public static final Target BOTON_GOOGLE_TRASLATE=Target.the("El botón de app Traductor")
			.located(By.id("gb51"));
	public static final Target BOTON_LENGUAJE_ORIGEN =Target.the("El boton de idioma origen")
			.located(By.id("gt-sl-gms"));
	public static final Target BOTON_LENGUAJE_DESTINO =Target.the("la boton de idioma destino")
			.located(By.id("gt-tl-gms"));
	
	public static final Target OPCION_INGLES =Target.the("La opción ingles")
			.located(By.xpath("//div[@id='gt-sl-gms-menu']/table/tbody//tr/td//div[contains(text(),'ingl')]"));
	
	public static final Target OPCION_ESPAÑOL =Target.the("La opción español")
			.located(By.xpath("//div[@id='gt-tl-gms-menu']/table/tbody//tr/td//div[contains(text(),'espa')]")); 
	        
		public static final Target AREA_DE_TRADUCCION =Target.the("Area de traducción")
			.located(By.id("source"));
	public static final Target BOTON_TRADUCIR=Target.the("El botón traducir")
			.located(By.id("gt-submit"));
	public static final Target AREA_TRADUCIDA=Target.the("Area donde se presenta la palabra traducida")
			.located(By.id("gt-res-dir-ctr"));
}


