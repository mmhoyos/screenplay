# language: es
#Author: your.email@your.domain.com


@regresion
Característica: Reportar mis actividades en el sistema de Registro Maxtime
  Como analista de pruebas
  Quiero realizar el reporte de mis actividades en MaxTime
	Para cumplir con la tarea que debo llevar a cabo diariamente

 
  @caso1
  Esquema del escenario: Title of your scenario outline
    Dado que Malory quiere ingresar el tiempo en maxtime
    |<usuario>|<contrasena>|
    Cuando ella ingresa las actividades del dia
    |<Proyecto>|
    Entonces ella deberia ver el dia correctamente cerrado

    Ejemplos: 
      | usuario |contrasena|Proyecto|
      | mmhoyos |Julio2015@|PMO25992| 

