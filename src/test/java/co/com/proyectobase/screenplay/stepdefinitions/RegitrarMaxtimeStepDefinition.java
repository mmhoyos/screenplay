package co.com.proyectobase.screenplay.stepdefinitions;

import org.openqa.selenium.WebDriver;
import co.com.proyectobase.screenplay.model.Usuario;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Autenticar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.userinterface.MaxTimeLoginPage;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

import java.util.List;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;

public class RegitrarMaxtimeStepDefinition  {
	
//	private MaxTimeLoginPage maxTimeLoginPage;
	
	@Managed(driver="Chrome")
	private WebDriver hisBrowser;
	private Actor malory =Actor.named("Malory");
	
	
	@Before
	public void ConfiguracionInicial(){
	malory.can(BrowseTheWeb.with(hisBrowser));
		
	}

	@Dado("^que Malory quiere ingresar el tiempo en maxtime$")
	public void que_Malory_quiere_ingresar_el_tiempo_en_maxtime(DataTable dtDatos) {
		malory.attemptsTo(Autenticar.EnLaAplicacionMaxtime(dtDatos));
	}

	@Cuando("^ella ingresa las actividades del dia$")
	public void ella_ingresa_las_actividades_del_dia(List<String> dtDatos) {
		malory.attemptsTo(Ingresar.LasActividadesDelDia(dtDatos));
	}

	@Entonces("^ella deberia ver el dia correctamente cerrado$")
	public void ella_deberia_ver_el_dia_correctamente_cerrado() throws Exception {
	}

	
	
	
	
	
}
