package co.com.proyectobase.screenplay.stepdefinitions;


import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefinition {
	
	@Managed(driver="Chrome")
	private WebDriver hisBrowser;
	private Actor malory =Actor.named("Malory");
	
	@Before
	public void ConfiguracionInicial(){
		malory.can(BrowseTheWeb.with(hisBrowser));
		
	}
	
	@Dado("^que Malory quiere usar el traductor de Google$")
	public void queMaloryQuiereUsarElTraductorDeGoogle()  {
		malory.wasAbleTo(Abrir.LaPaginaDeGoogle());
	}


	@Cuando("^el traduce la palabra (.*) de ingles a español$")
	public void elTraduceLaPalabraTableDeInglesAEspañol(String palabra) {
		malory.attemptsTo(Traducir.DeInglesAEspanolLa(palabra));
	}

	@Entonces("^ella deberia ver la palabra (.*) en la pantalla$")
	public void ellaDeberiaVerLaPalabraMesaEnLaPantalla(String palabraEsperada) {
		malory.should(seeThat(LaRespuesta.es(),equalTo(palabraEsperada)));
	   
	}
	
}
